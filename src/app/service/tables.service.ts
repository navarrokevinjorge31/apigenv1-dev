import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TablesService {

  private baseurl = 'http://103.29.250.173:8080/genapi1/';
  // private baseurl = 'https://localhost/genapi1/';

  constructor(private http: HttpClient) { }

  getAllDatasets(code:any){
    return this.http.get<Array<any>>(this.baseurl + code +'/api/tables');
  }
}
