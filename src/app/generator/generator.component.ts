import { Component, OnInit } from '@angular/core';
import { TablesService } from '../service/tables.service';

interface City {
  name: string,
  code: string
}

@Component({
  selector: 'app-generator',
  templateUrl: './generator.component.html',
  styleUrls: ['./generator.component.css']
})

export class GeneratorComponent implements OnInit {

  cities: City[];
  selectedCity: City;

  public dataset: any;
  httprequest: string;
  tables: string[] = [];
  post: string = 'http://103.29.250.173:8080/genapi1/';
  get: string = 'http://103.29.250.173:8080/genapi1/';
  // post: string = 'http://localhost/genapi1/';
  // get: string = 'http://localhost/genapi1/';
  public code: any;
  key: string;
  value: string;

  constructor(private tableservice: TablesService ) {

    this.cities = [
      {name: 'General Santos City', code: 'gsc'},
      {name: 'GSC Datamart', code: 'gscmart'},
      {name: 'Manila City', code: 'mnl'},
      {name: 'UP Project 3', code: 'up'},
  ];

  }

  ngOnInit(): void {
    
  }

  getAllDatasets(code1:any){
    this.tableservice.getAllDatasets(code1).subscribe(res => {
      this.dataset = res;
      //console.log(this.dataset);
      
    })
  }

  onChange(code:any){
    this.getAllDatasets(code);
    console.log(code);
  }

}
